﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBall : MonoBehaviour
{
    public BallController ballController;
    bool called = false;
    public GameObject UIEffect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !called)
        {
            called = true;
            ballController.startRun();

            UIEffect.SetActive(true);
            StartCoroutine(wait5Seconds());
        }
    }

    IEnumerator wait5Seconds()
    {
        yield return new WaitForSeconds(2.0f);
        UIEffect.SetActive(false);
    }
}
