﻿using UnityEngine;
using System.Collections;

public class AutoMove2 : MonoBehaviour
{
    UnityEngine.AI.NavMeshAgent ball;
    public Transform[] goals;
    Vector3 curGoal;
    // Use this for initialization
    void Start()
    {
        ball = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        goals = GameObject.Find("SphereGoal").GetComponentsInChildren<Transform>();
        curGoal = goals[0].position;
        ball.SetDestination(curGoal);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(curGoal, this.transform.position) < 70.0f)
        {
            curGoal = goals[Random.Range(1, 5)].position;
            ball.SetDestination(curGoal);
        }
    }
}
