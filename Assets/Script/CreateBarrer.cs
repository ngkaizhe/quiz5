﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBarrer : MonoBehaviour
{
    public GameObject ball;
    public Transform RollerBallGoal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 target = transform.position;
            target.y = 5f;
            GameObject obj = Instantiate(ball, target, Quaternion.identity);
            obj.GetComponent<BallController>().Goal = RollerBallGoal;
        }
    }
}
