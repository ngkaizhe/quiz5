﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public Transform Goal;
    UnityEngine.AI.NavMeshAgent ball;
    // Start is called before the first frame update
    void Start()
    {
        ball = GetComponent<UnityEngine.AI.NavMeshAgent>();
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void startRun()
    {
        ball.SetDestination(Goal.position);
    }
}
